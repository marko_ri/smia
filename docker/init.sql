CREATE DATABASE IF NOT EXISTS ostock_dev;
USE ostock_dev;

CREATE TABLE IF NOT EXISTS organizations
(
    organization_id varchar(32) NOT NULL,
    name varchar(32),
    contact_name varchar(32),
    contact_email varchar(32),
    contact_phone varchar(32),
    CONSTRAINT organizations_pkey PRIMARY KEY (organization_id)
);

CREATE TABLE IF NOT EXISTS licenses
(
    license_id varchar(32) NOT NULL,
    organization_id varchar(32) NOT NULL,
    description varchar(32),
    product_name varchar(32) NOT NULL,
    license_type varchar(32) NOT NULL,
    comment varchar(32),
    CONSTRAINT licenses_pkey PRIMARY KEY (license_id),
    CONSTRAINT licenses_organization_id_fkey FOREIGN KEY (organization_id)
        REFERENCES organizations (organization_id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
